import React, { useState } from 'react';
import Error from './error'
import shortid from 'shortid'
import PropTypes from 'prop-types'


const Formulario = ({ guardarGasto , guardarCrearGasto}) => {

    const [nombre, guardarNombre] = useState('')
    const [cantidad, guardarCantidad] = useState(0)
    const [error, guardarError] = useState(false)


    const agregarGasto = e =>{
        e.preventDefault();

        //validar 
        if (cantidad < 1 || isNaN(cantidad) || nombre.trim() === '') {
            guardarError(true)
            return
        }
        guardarError(false)

        //construir 

        const gasto = {
            nombre,
            cantidad,
            id : shortid.generate()
        }

        //enviar al app 
        guardarGasto(gasto)
        guardarCrearGasto(true)


        //resetear
        guardarCantidad(0)
        guardarNombre('')
    }
    return (
        <form onSubmit = {agregarGasto}>
            <h2>Agrega tus gastos</h2>
            {error ? <Error mensaje = "monto invalido o campo obligatorio vacio"/> : null}
            <div className="campo">
                <label>Nombre Gasto</label>
                <input
                    type="text"
                    className="u-full-width"
                    placeholder="Ej Transporte"
                    value={nombre}
                    onChange={e => guardarNombre(e.target.value)}
                />
            </div>
            <div className="campo">
                <label>Cantidad Gasto</label>
                <input
                    type="number"
                    className="u-full-width"
                    placeholder="Ej 300"
                    value={cantidad}
                    onChange={e => guardarCantidad(parseInt(e.target.value, 10))}

                />
            </div>
            <input
                type="submit"
                className="button-primary u-full-width"
                value="agregar Gasto"


            />
        </form>
    );
}

Formulario.prototype = {
    guardarGasto : PropTypes.func.isRequired,
    guardarCrearGasto : PropTypes.func.isRequired,
}


export default Formulario;