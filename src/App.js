import React, { Fragment, useState , useEffect } from 'react';
import Pregunta from './component/pregunta'
import Formulario from './component/Formulario'
import Listado from './component/Listado'
import ControlPresupuesto from './component/ControlPresupuesto'


function App() {

  // state presupuesto 

  const [presupuesto, guardarPresupuesto] = useState(0)
  const [restante, guardarRestante] = useState(0)
  const [mostrarpregunta, actualizarPregunta] = useState(true)
  const [gastos, guardarGastos] = useState([])
  const [gasto , guardarGasto] = useState({})
  const [creargasto , guardarCrearGasto] = useState(false)


  // use efect para actualizar el restante 

  useEffect(() => {
    if(creargasto){
      guardarGastos([
        ...gastos,
        gasto
      ])

      guardarCrearGasto(false)
      const presupuestoRestante = restante - gasto.cantidad
      guardarRestante(presupuestoRestante)
    }

  }, [gasto , creargasto , gastos , restante])


  return (
    <Fragment>
      <div className="container">
        <header>
          <div className="contenido-principal contenido">
            {mostrarpregunta ?
              <Pregunta
                guardarPresupuesto={guardarPresupuesto}
                guardarRestante={guardarRestante}
                actualizarPregunta={actualizarPregunta}
              /> : (
                <div className="row">
                  <div className="one-half column">
                    <Formulario
                      guardarGasto={guardarGasto}
                      guardarCrearGasto = {guardarCrearGasto}
                    />
                  </div>
                  <div className="one-half column">
                    <Listado
                      gastos = {gastos}
                    />
                    <ControlPresupuesto
                      presupuesto = {presupuesto}
                      restante = {restante}
                    />
              </div>
                </div>

              )}
          </div>
        </header>
      </div>

    </Fragment>
  );
}

export default App;
